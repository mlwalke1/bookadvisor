package edu.svsu.bookadvisor;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

public class FIndBookActivity extends Activity {
    private BookExpert expert = new BookExpert();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_book);
    }

    //Call when the button gets clicked
    public void onClickFindBook(View view) {
        //Get a reference to the TextView
        TextView books = (TextView) findViewById(R.id.books);

        //Get a reference to the Spinner
        Spinner genre = (Spinner) findViewById(R.id.genre);

        //Get the selected item in the Spinner
        String bookType = String.valueOf(genre.getSelectedItem());

        //Get recommendations from the BeerExpert class
        List<String> booksList = expert.getBooks(bookType);
        StringBuilder booksFormatted = new StringBuilder();
        for (String book : booksList) {
            booksFormatted.append(book).append('\n');
        }

        //Display the beers
        books.setText(booksFormatted);
    }
}
