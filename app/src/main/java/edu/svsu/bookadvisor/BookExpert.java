package edu.svsu.bookadvisor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by davidg on 28/04/2017.
 */

public class BookExpert {
    List<String> getBooks(String genre) {
        List<String> books = new ArrayList<>();
        if (genre.equals("Science Fiction")) {
            books.add("Ready Player One");
            books.add("The Martian");
        } else if (genre.equals("Fantasy")) {
            books.add("The Hobbit");
            books.add("Game of Thrones");
            books.add("Harry Potter and the Sorcerer's Stone");
        } else if (genre.equals("Mystery")) {
            books.add("Murder on the Orient Express");
            books.add("The Da Vinci Code");
        } else if (genre.equals("Classic")) {
            books.add("The Catcher in the Rye");
            books.add("Frankenstein");
            books.add("The Great Gatsby");
            books.add("Nineteen Eighty-Four");
        }
        return books;
    }
}
